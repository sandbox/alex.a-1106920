This module is a drupal front end to mpc (http://linux.die.net/man/1/mpc), which in turn is a Linux command line interface to mpd (music player daemon).
Use this module to view and control your mpd music player.  It is intended to be used in a 'home' installation of drupal.

The main page (/mpc) displays the currently playing song and a few songs before and after.
It updates automatically at the end of each song.

Other Features:
* Search songs by keyword
* Browse song folders
* Put the search or browse results in the current playlist
* List/Save/Load/Delete playlists
* Select the song to play from the current playlist
* Shuffle the current playlist

The module keeps no user data in the drupal database.
It copies song metadata from mpd to the drupal database for easier access (mpc provides metadata only for the current playlist).
Playlists are stored and retrieved in mpd.
It has two permissions:
'mpc view' only allows viewing  the currently playing songs.
'mpc admin' allows access to everything else.  

 To get started:
 * Make sure the program "mpc" is configured properly and is in drupal's path.
 * Turn mpd on and verify that the shell command line "mpc status" displays the current song. 
 * Enable the Music Player module and give yourself all the mpc permissions.
 * Go to the page /mpc.  You should see the currently playing songs.
 * Go to the page /mpc/db.  This copies the song metadata from mpd to drupal.
 * Browse your music and listen to it. 
