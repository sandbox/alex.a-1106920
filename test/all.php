<?php

require_once('MPCTest.inc');

class AllTests extends GroupTest {
  function AllTests() {
    $this->addTestCase( new MPCTest());
  }
}
$test = new AllTests();
if ( SimpleReporter::inCli()) {
  exit($test->run(new TextReporter()) ? 0 : 1);
} else {
  $test->run(new HtmlReporter());
}