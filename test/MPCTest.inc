<?php

/**
 * @file
 * Unit Tests
 */

require_once('simpletest/unit_tester.php');
require_once('simpletest/reporter.php');
require_once('../mpc.module');
require_once('../mpc.inc');

class MPCTest extends UnitTestCase {

  function test_lastFolder() {
    $last = MPC::lastFolder("a1/b1/c1/d1");
    $this->assertEqual("d1", $last);
  }
}
