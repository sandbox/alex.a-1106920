<?php

require_once 'mpd.core.inc';
/**
 * @file
 * Implementation of mpc pages.
 */


/**
 * Metada for one song.
 */
class MPCSong {

  public $album;
  public $artist;
  public $file;
  public $title;

  public function gettitle() {
    return $this->title ? $this->title : $this->file;
  }

  public function folder() {
    $pos = strrpos($this->file, '/');
    if ($pos >= 0) {
      return substr($this->file, 0, $pos);
    }
    else {
      return NULL;
    }
  }
  
  /** Convert an array representing an mpd song to an MPDSong object. */
  public static function fromMpd($array) {
    $song = new MPCSong();
    $map = array(
        "Album" => "album",
        "Artist" => "artist",
        "Title" => "title",
        "file" => "file",
        "Time" => "time",
        "Track" => "track",         
        );
    foreach( $map as $key => $field ) {
      if ( $field && key_exists( $key, $array ) ) {
        $song->$field = $array[$key];
      }
    }
    return $song;
  }
  
  /** Convert an array of song arrays to an array of song objects */
  public static function fromMpdArray($array) {
    $songs = array();
    foreach( $array as $file ) {
      $songs[] = self::fromMpd($file);
    }
    return $songs;
  }
}

/**
 * static functions for interfacing with mpd.
 */
class MPC {
  static private $mpd;

  static $current_file;

  static private function getMpd() {
    if ( ! self::$mpd ) {
      $host = variable_get( 'mpc_host', 'localhost');
      $port = variable_get( 'mpc_port', '6600');
      self::$mpd = new MPDCore($host, $port);
    }
    return self::$mpd;
  }
  
  static public function close() {
    if ( self::$mpd ) {
      self::$mpd->Disconnect();
    }
    self::$mpd = NULL;
  }

  static function escapeFolder($folder) {
    $parts = split('/', $folder);
    return implode('::', $parts);  
  }
  
  static function lastFolder($folder) {
    $pos = strrpos($folder, '/');
    if ( $pos )
      return substr( $folder, $pos + 1 );
    return $folder;
  }

  static function unescapeFolder($path) {
    if ( $path == '/' )
      return '';
    $parts = split('::', $path);
    return implode('/', $parts);  
  }
  
  static public function listsToSongs($lists) {    
    if ( $lists && array_key_exists('files', $lists)) {
      return MPCSong::fromMpdArray($lists['files']);
    }
    return array();
  }
  
  /**
    Call mpc status and return a status object.
   */
  public static function status() {
    $status = new stdClass();
    $mpd = self::getMpd();
    $mpd_status = $mpd->parseArray($mpd->SendCommand('status'));
    $status->playlist_position = (int) $mpd_status['song'];
    $status->playlist_size = (int) $mpd_status['playlistlength'];
    $status->song_position = (float) $mpd_status['elapsed'];

    $mpd_currentsong = $mpd->parseArray($mpd->SendCommand("currentsong"));
    $status->consume = $mpd_status['consume'];
    $status->song_size = (float) $mpd_currentsong['Time'];
    
    $status->file = self::$current_file = $mpd_currentsong['file'];
    $status->volume = (int) $mpd_status['volume'];
    $status->repeat = $mpd_status['repeat'];
    $status->random = $mpd_status['random'];
    $status->single = $mpd_status['single'];
    return $status;
  }

  static function current() {
    if (!self::$current_file) {
      $mpd_currentsong = mpd::parseArray(self::$mpd->SendCommand("currentsong"));
      self::$current_file = $mpd_currentsong['file'];
    }
    return self::$current_file;
  }

  static function playlists() {
    $mpd = self::getMpd();
    $resp = $mpd->SendCommand("listplaylists");
    self::close();
    $items = $mpd->parseItems($resp);
    if ( $items && array_key_exists( 'playlists', $items ))
      return $items['playlists'];
    else
      return array();
  }

  public static function playlist($playlist = NULL) {
    $mpd = self::getMpd();
    $resp = NULL;
    if ( $playlist ) {
      $resp = $mpd->SendCommand("listplaylistinfo", $playlist);
    } else {
      $resp = $mpd->SendCommand("playlistinfo");
    }
    $cmd = "playlistinfo";
    $args = array();
    $mpd_playlist = $mpd->parseFiles($resp);
    $songs = MPCSong::fromMpdArray($mpd_playlist);
    self::close();
    return $songs;
  }

  public static function allSongs() {
    $mpd = self::getMpd();
    $files = $mpd->parseFiles($mpd->SendCommand("listallinfo"));
    self::close();
    $songs = MPCSong::fromMpdArray($files);
    return $songs;
  }
  
  static function search($text) {
    $mpd = self::getMpd();
    $files = $mpd->parseFiles($mpd->SendCommand("search", "any", $text ));
    self::close();
    return MPCSong::fromMpdArray($files);
  }
  
  static function load($playlist) {
    $mpd = self::getMpd();
    $mpd->SendCommand( "load", $playlist );
    self::close();
  }

  static function playlist_play($playlist) {
    $mpd = self::getMpd();
    $commands = array();
    $commands[] = array( "clear" );
    $commands[] = array( "load", $playlist );
    $commands[] = array( "play", 0 );
    $mpd->SendCommands($commands);
    self::close();
  }

  static function save($playlist) {
    $mpd = self::getMpd();
    $mpd->SendCommand( "save", $playlist );
    self::close();
  }

  static function rm($playlist) {
    $mpd = self::getMpd();
    $mpd->SendCommand( "rm", $playlist );
    self::close();
  }

  static function clear() {
    $mpd = self::getMpd();
    $mpd->SendCommand( "clear" );
    self::close();
  }

  static function next() {
    $mpd = self::getMpd();
    $mpd->SendCommand( "next" );
    self::close();
  }

  static function prev() {
    $mpd = self::getMpd();
    $mpd->SendCommand( "previous" );
    self::close();
  }

  static function shuffle() {
    $mpd = self::getMpd();
    $mpd->SendCommand( "shuffle" );
    self::close();
  }

  static function stop() {
    $mpd = self::getMpd();
    $mpd->SendCommand( "stop" );
    self::close();
  }

  static function crop() {
    // there is no mpd crop command.
    // we need to delete all songs from the current playlist, except for the current song.
    $status = self::status();
    $mpd = self::getMpd();
    if ( $status->playlist_size > 1 ) {
      $commands = array();
      if ( $status->playlist_position + 2 < $status->playlist_size )
        $commands[] = array( "delete", ($status->playlist_position + 1) . ":" . $status->playlist_size);
      if ( $status->playlist_position > 0 ) {
        $commands[] = array( "delete", "0:" . $status->playlist_position);        
      }
      $mpd->SendCommands($commands);
    }
    self::close();
  }

  static function play($position) {
    $position = (int) $position;
    $mpd = self::getMpd();
    $mpd->SendCommand( "play", $position );
    self::close();
  }

  static function add_files($files) {
    $commands = array();
    foreach ($files as $file) {
      $commands[] = array( "add", $file );
    }
    $mpd = self::getMpd();
    $mpd->SendCommands( $commands );
    self::close();
  }

  static function add_songs($songs) {
    $files = array();
    foreach ($songs as $song) {
      $files[] = $song->file;
    }
    self::add_files($files);
  }

  static function ls($path) {
    $mpd = self::getMpd();
    $resp = $mpd->SendCommand("lsinfo", $path);
    self::close();
    $dir = $mpd->parseItems($resp);
    return $dir;
  }

}

function mpc_format_duration($time) {
  return sprintf( "%d:%02d", $time / 60, $time % 60);
}
/**
 * Display a table of songs.
 * @param array $songs array of MPCSong
 * @param int $offset The index of the first song to display.
 * @param int $limit The max number of songs to display, or NULL for no limit.
 * @param current The filename of the currently playing song.  It is marked if it's in the table.
 */
function mpc_song_table($songs, $offset = 0, $length = NULL, $current = NULL) {
  $count = count($songs);
  if (!$length)
    $length = $count - $offset;
  if ( $offset + $length >= $count)
    $length = $count - $offset;
  $header = array(t('Album'), t('Artist'), t('Title'), t('Duration'));
  $rows = array();
  if ($offset != 0)
    $rows[] = array('...', '...', '...', '...');
  $pos = $offset;
  for ($i = 0; $i < $length; $i++) {
    $song = $songs[$offset + $i];
    $pos = $offset + $i;
    $title = $song->gettitle();
    if ($current && user_access('mpc admin')) {
      if ($song->file == $current) {
        $title = l($title, "mpc/play/$pos", array('attributes' => array('name' => 'playing')));
      }
      else {
        $title = l($title, "mpc/play/$pos");
      }
    }
    if ($song->file == $current) {
      $title = ' * ' . $title;
    }
    $album = htmlspecialchars($song->album);
    $rows[] = array(
        $album,
        htmlspecialchars($song->artist),
        $title,
        mpc_format_duration($song->time)
    );
  }
  if ($offset + $length != count($songs))
    $rows[] = array('...', '...', '...', '...');
  return theme_table($header, $rows);
}

function mpc_linkmenu($menu) {
  return implode(' | ', $menu);
}

/**
 * The current playlist page.
 */
function mpc_playlist($limit) {
  $status = MPC::status();
  if ($status->song_size > 0) {
    // refresh the page at the end of the song.
    $time_left = $status->song_size - $status->song_position + 1; // add a second to make sure we're on the next song.
    drupal_set_html_head('<meta http-equiv="refresh" content="' . $time_left . '"/>');
  }
  $songs = MPC::playlist();
  $size = count($songs);
  if ($limit && $size) {
    $offset = $status->playlist_position - $limit / 2;
    if ($offset < 0)
      $offset = 0;
    if ($offset + $limit > $size)
      $limit = $size - $offset;
  }
  $begin = NULL;
  $end = NULL;
  if (user_access('mpc admin')) {
    if ($limit != $size)
      $end = l(t('See All'), 'mpc/playlist');
    $top_menu = array(
        l(t('Folders'), 'mpc/ls'),
        l(t('Search'), 'mpc/search'),
        l(t('Playlists'), 'mpc/playlists'),
    );
    
    $bottom_menu = array(

        l(t('Next'), 'mpc/next'),
        l(t('Previous'), 'mpc/prev'),
        l(t('Shuffle'), 'mpc/shuffle'),
        l(t('Crop'), 'mpc/crop'),
        l(t('Stop'), 'mpc/stop'),
        l(t('Play'), 'mpc/play/0'),
    );
    $begin = mpc_linkmenu($top_menu) . '<br>' . mpc_linkmenu($bottom_menu);
  }
  drupal_set_breadcrumb(array());
  return $begin . mpc_song_table($songs, $offset, $limit, $status->file) . $end;
}

/**
 * The playlists page.
 */
function mpc_playlists() {
  drupal_set_breadcrumb(array(
      l(t('Music Player'), 'mpc'),
  ));
  $playlists = MPC::playlists();
  $header = array(t('Playlist'));
  $rows = array();
  foreach ($playlists as $playlist) {
    $rows[] = array(
        l($playlist, "mpc/pl/list/$playlist"),
        l(t('Play'), "mpc/pl/play/$playlist"),
        l(t('Add'), "mpc/pl/load/$playlist"),
        l(t('Delete'), "mpc/pl/rm/$playlist"),
    );
  }
  return drupal_get_form('mpc_save_form') . theme_table($header, $rows);
}

/**
 * The form for saving a playlist.
 */
function mpc_save_form($form_state) {
  $form['name'] = array(
      '#type' => 'textfield',
      '#title' => 'Save Current Playlist As',
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
  );
  return $form;
}

function mpc_save_form_submit($form, &$form_state) {
  $values = $form['#post'];
  $name = $values['name'];
  MPC::save($name);
  drupal_goto("mpc/playlists");
}

function mpc_playlist_play($playlist) {
  MPC::playlist_play($playlist);
  drupal_goto('mpc');
}

function mpc_load($playlist) {
  MPC::load($playlist);
  drupal_goto('mpc');
}

function mpc_rm($playlist) {
  MPC::clear();
  MPC::rm($playlist);
  drupal_goto('mpc/playlists');
}

function mpc_playlist_list($playlist) {
  $songs = MPC::playlist($playlist);
  return mpc_song_table($songs);
}


/**
 * A control page used to do various operations.
 */
function mpc_control($function) {
  MPC::$function();
  drupal_goto('mpc');
}

function mpc_play($position) {
  MPC::play($position);
  drupal_goto('mpc');
}

function mpc_clear() {
  MPC::clear();
  return NULL;
}

/**
 * The form for searching for songs.
 */
function mpc_search_form($form_state, $search = NULL) {
  $form['search'] = array(
      '#type' => 'textfield',
      '#title' => 'Search',
      '#value' => $search,
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Search'),
  );
  return $form;
}

function mpc_search_form_submit($form, &$form_state) {
  $values = $form['#post'];
  $text = $values['search'];
  drupal_goto("mpc/search/$text");
}

/**
 * Processing of search results.
 */
function mpc_search($mode, $search = NULL) {
  drupal_set_breadcrumb(array(
      l(t('Music Player'), 'mpc'),
  ));
  $result = NULL;
  $menu = NULL;
  if ($search) {
    $songs = MPC::search($search);
    if ($songs) {
      if ($mode == 'replace') {
        MPC::clear();
        MPC::add_songs($songs);
        MPC::play(0);
        drupal_goto('mpc/playlist');
      }
      elseif ($mode == 'cropadd') {
        MPC::crop();
        MPC::add_songs($songs);
        drupal_goto('mpc/playlist');
      }
      elseif ($mode == 'add') {
        MPC::add_songs($songs);
        drupal_goto('mpc/playlist');
      }
      $menu = '<hr>' . mpc_linkmenu(array(
                  l(t('Replace Playlist'), "mpc/search/replace/$search"),
                  l(t('Add to Playlist'), "mpc/search/add/$search"),
                  l(t('Crop and Add'), "mpc/search/cropadd/$search"),
              ));
    }
    $result = mpc_song_table($songs);
  }
  return drupal_get_form('mpc_search_form', $search) . $menu . $result;
}

/**
 *
 * The list page.
 * @param string $mode A mode argument tellng us what to do with the folder.
 * @param string $path The folder, escaped.  path separators ('/') are replaced with '::'.  There is no initial path separator.
 */
function mpc_ls($mode, $path) {
  $folder = MPC::unescapeFolder($path);
  $path = MPC::escapeFolder($folder);
  $bread = array(
      l(t('Music Player'), 'mpc'),
      $bread[] = l(t('[Folders]'), "mpc/ls")
  );

  $parts = array();
  if ( $folder )
    $parts = explode( '/', $folder );
  $count = count($parts);
  if ( $count > 0 )
    drupal_set_title( $parts[$count-1]);
  for( $i = 0; $i < $count - 1; $i++ ) {
    $f = implode('/', array_slice( $parts, 0, $i + 1));
    $p = MPC::escapeFolder($f);
    $bread[] = l($parts[$i], "mpc/ls/$p");
  }

  if ($mode == 'replace') {
    MPC::clear();
    MPC::add_files(array($folder));
    MPC::play(0);
    drupal_goto('mpc/playlist');
  }
  elseif ($mode == 'cropadd') {
    MPC::crop();
    MPC::add_files(array($folder));
    drupal_goto('mpc/playlist');
  }
  elseif ($mode == 'add') {
    MPC::add_files(array($folder));
    drupal_goto('mpc/playlist');
  }

  $items = MPC::ls($folder);
  $directories = $items['directories'];
  $songs = MPC::listsToSongs($items);
  
  $list = array();
  foreach( $directories as $dir ) {
    $list[] = l(MPC::lastFolder($dir), "mpc/ls/" . MPC::escapeFolder($dir));    
  }
  $out = FALSE;
  $out .= '<hr>' . mpc_linkmenu(array(
              l(t('Replace Playlist'), "mpc/ls/replace/$path"),
              l(t('Add to Playlist'), "mpc/ls/add/$path"),
              l(t('Crop and Add'), "mpc/ls/cropadd/$path"),
          ));
  $out .= theme_item_list($list);
  if (count($songs) > 0) {
    $out .= mpc_song_table($songs);
  }
  drupal_set_breadcrumb($bread);
  return $out;
}

function mpc_admin_form() {
  $form = array();
  $form['mpc_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => variable_get( 'mpc_host', 'localhost'),
    '#maxlength' => 60,
    '#description' => t('The MPD daemon host.'),
  );
  $form['mpc_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get( 'mpc_port', '6600'),
    '#maxlength' => 5,
    '#description' => t('The MPD daemon port.'),
    '#after_build' => array('mpc_check_port'),
  );
  return system_settings_form($form);
}

function mpc_check_port($form_element) {
  $value = $form_element['#value'];
  if ( ! is_numeric($value)) {
    $form_item = $form_element['#parents'][0];
    form_set_error($form_item, t('Not a number: %value.', array('%value' => $value)));
  } 
  return $form_element;
}

