<?php

/* 
 * Alex Athanasopoulos (alex@melato.org) 07/2012
 * This class implements the base mpd communcation protocol.
 * It is based on mpd class.
 * It includes the basic communication with mpd, including how to send commands, retrieve and parse responses, etc.
 * It does not include any specific commands (such as retrieving playlists, status, etc.
 */


class MPDCore {
    const MPD_RESPONSE_ERR = "ACK";
    const MPD_RESPONSE_OK = "OK";
    const MPD_CMD_PASSWORD = "password";
    const MPD_CMD_START_BULK = "command_list_begin";
    const MPD_CMD_END_BULK = "command_list_end";
    
	// TCP/Connection variables
	var $host;
	var $port;
    var $password;

	var $mpd_sock   = NULL;
	var $connected  = FALSE;

	// MPD Status variables
	var $mpd_version    = "(unknown)";

    // =================== BEGIN OBJECT METHODS ================

	/* mpd() : Constructor
	 * 
	 * Builds the MPD object, connects to the server, and refreshes all local object properties.
	 */
	function __construct($srv,$port,$pwd = NULL) {
		$this->host = $srv;
		$this->port = $port;
        $this->password = $pwd;
        
    	$resp = $this->Connect();
		if ( is_null($resp) ) {
			return;
		} else {
			list ( $this->mpd_version ) = sscanf($resp, self::MPD_RESPONSE_OK . " MPD %s\n");
            if ( ! is_null($pwd) ) {
                if ( is_null($this->SendCommand(self::MPD_CMD_PASSWORD,$pwd)) ) {
                    $this->connected = FALSE;
                    $this->addErr("bad password");
                    return;  // bad password or command
                }
            }
		}
	}
	
	/* Connect()
	 * 
	 * Connects to the MPD server. 
     * 
	 * NOTE: This is called automatically upon object instantiation; you should not need to call this directly.
	 */
	function Connect() {
		$this->mpd_sock = fsockopen($this->host,$this->port,$errNo,$errStr,10);
		if (!$this->mpd_sock) {
			$this->addErr("Socket Error: $errStr ($errNo)");
			return NULL;
		} else {
			$counter=0;
			while(!feof($this->mpd_sock)) {
				$counter++;
				if ($counter > 10){
					$this->addErr("no file end");
					return NULL;
				}
				$response =  fgets($this->mpd_sock,1024);
								
				if (strncmp(self::MPD_RESPONSE_OK,$response,strlen(self::MPD_RESPONSE_OK)) == 0) {
					$this->connected = TRUE;
					return $response;
				}
				if (strncmp(self::MPD_RESPONSE_ERR,$response,strlen(self::MPD_RESPONSE_ERR)) == 0) {
					// close socket
					fclose($this->mpd_sock);
					$this->addErr("Server responded with: $response");
					return NULL;
				}

			}
			// close socket
			fclose($this->mpd_sock);
			// Generic response
			$this->addErr("Connection not available");
			return NULL;
		}
	}

    private function _createCommandLine($cmd, $args ) {
        $cmdStr = $cmd;
        foreach( $args as $arg ) {
          if ($arg && strlen($arg) > 0) $cmdStr .= " \"$arg\"";
        }
        return $cmdStr;        
    }

    function addErr($str) {
      // called for error messages.  The base implementation does nothing.
      // but you may override this to redirect error messages somewhere.
    }
    
	/* SendCommand()
	 * 
	 * Sends a generic command to the MPD server. Several command constants are pre-defined for 
	 * use (see self::MPD_CMD_* constant definitions above). 
	 */
	function SendCommand($cmd /* var-args */) {
      $cmdStr = $cmd;

		// Clear out the error String
		$this->errStr = NULL;
		$respStr = "";

		if ( ! $this->connected ) {
			$this->addErr( "mpd->SendCommand() / Error: Not connected");
		} else {
            $cmdStr = $this->_createCommandLine($cmd, array_slice(func_get_args(), 1) );

			fputs($this->mpd_sock,"$cmdStr\n");
			while(!feof($this->mpd_sock)) {
				$response = fgets($this->mpd_sock,1024);
				
				// An OK signals the end of transmission -- we'll ignore it
				if (strncmp(self::MPD_RESPONSE_OK,$response,strlen(self::MPD_RESPONSE_OK)) == 0) {
					break;
				}

				// An ERR signals the end of transmission with an error! Let's grab the single-line message.
				if (strncmp(self::MPD_RESPONSE_ERR,$response,strlen(self::MPD_RESPONSE_ERR)) == 0) {
					list ( $junk, $errTmp ) = strtok(self::MPD_RESPONSE_ERR . " ",$response );
					$this->addErr( strtok($errTmp,"\n") );
					return NULL;
				}

				// Build the response string
				$respStr .= $response;
			}
		}
		return $respStr;
	}

    /**
     * Send multiple arrays.
     * @param type an array of commands
     * Each command is an array where the first element is the command name, and the remaining elements are the arguments.
     */
    function SendCommands( $cmdArray ) {
      $lines = array();
      $lines[] = self::MPD_CMD_START_BULK;
      foreach( $cmdArray as $cmd ) {
        $lines[] = $this->_createCommandLine($cmd[0], array_slice($cmd,1));
      }
      $lines[] = self::MPD_CMD_END_BULK;
      $cmdStr = implode("\n", $lines);
      return $this->SendCommand($cmdStr);
    }

	/* Disconnect() 
	 * 
	 * Closes the connection to the MPD server.
	 */
	function Disconnect() {
		if ( $this->debugging ) echo "mpd->Disconnect()\n";
		fclose($this->mpd_sock);

		$this->connected = FALSE;
		unset($this->mpd_version);
		unset($this->mpd_sock);
	}


	//*******************************************************************************//
	//***************************** INTERNAL FUNCTIONS ******************************//
	//*******************************************************************************//

    /* _computeVersionValue()
     *
     * Computes a compatibility value from a version string
     *
     */
    private function _computeVersionValue($verStr) {
		list ($ver_maj, $ver_min, $ver_rel ) = explode(".",$verStr);
		return ( 100 * $ver_maj ) + ( 10 * $ver_min ) + ( $ver_rel );
    }

	/*
	 * checks the file entry and complete it if necesarry
	 * checked fields are 'Artist', 'Genre' and 'Title' 
	 *
	 */
	private function _validateFile( $fileItem ){

		$filename = $fileItem['file'];

		if (!isset($fileItem['Artist'])){ $fileItem['Artist']=null; }
		if (!isset($fileItem['Genre'])){ $fileItem['Genre']=null; }
		
		// special conversion for streams 				
		if (stripos($filename, 'http' )!==false){
			if (!isset($fileItem['Title'])) $title = ''; else $title=$fileItem['Title'];
			if (!isset($fileItem['Name'])) $name = ''; else $name=$fileItem['Name'];
			if (!isset($fileItem['Artist'])) $artist = ''; else $artist=$fileItem['Artist'];
			
			if (strlen($title.$name.$artist)==0){
				$fileItem['Title'] = $filename;
			} else {
				$fileItem['Title'] = 'stream://'.$title.' '.$name.' '.$artist;	
			}
			
		}
				 				
		if (!isset($fileItem['Title'])){ 
			$file_parts = explode('/', $filename);
			$fileItem['Title'] = $filename;
		 }
				
		return $fileItem;		
	}
	
	/*
	 * take the response of mpd and split it up into
	 * items of types 'file', 'directory' and 'playlist' 
	 * 
	 */
	private function _extractItems( $resp ){
	
		if ( $resp == null ) {
			return NULL;
		} 
		
		// strip unwanted chars
		$resp = trim($resp);
		// split up into lines 
		$lineList = explode("\n", $resp );
		
		$array = array();
		
		$item=null;
		foreach ($lineList as $line ){
			list ( $element, $value ) = explode(": ",$line);

			
			// if one of the key words come up, store the item
			if (($element == "directory") or ($element=="playlist") or ($element=="file")){
				if ($item){
					$array[] = $item;
				}
				$item = array();
			}
			$item[$element] = $value;								
		}
		// check if there is a last item to store
		if (sizeof($item)>0){
			$array[] = $item;
		}
		
		return $array;			
	}
	
	
	/* parseFileListResponse() 
	 * 
	 * Builds a multidimensional array with MPD response lists for directories, files, playlists.
	 */
	public function parseItems($resp) {
		
		$valuesArray = $this->_extractItems( $resp );
		
		if ($valuesArray == null ){
			return null;
		}
		
		
		//1. create empty arrays
		$directoriesArray = array();
		$filesArray = array();
		$playlistsArray = array();
		

		//2. sort the items 		
		foreach ( $valuesArray as $item ) {
			
			if (isset($item['file'])){
				$filesArray[] = $this->_validateFile($item);
			} else if (isset($item['directory'])){
				$directoriesArray[] = $item['directory'];
			} else if (isset($item['playlist'])){
				$playlistsArray[] = $item['playlist'];	
			} else {
				$this->addErr('should not enter this');
			}
		} 
		
		//3. create a combined list of items		
		$returnArray = array(
							"directories"=>$directoriesArray,
							"playlists"=>$playlistsArray,
							"files"=>$filesArray
						);
		
				
		return $returnArray;

	}

	public function parseFiles($resp) {
      $lists = $this->parseItems($resp);
      if ( $lists && array_key_exists( 'files', $lists )) {
        return $lists['files'];
      }
      return array();
    }
    
    public function parseArray( $string ) {
      $array = array();
      if ( ! $string )
        return $array;
      $line = strtok($string,"\n");
      while( $line ) {
          list ( $key, $value ) = explode(": ",$line);
          $array[$key] = $value;
          $line = strtok("\n");
      }
      return $array;
    }
    public static function sort_filenames($a,$b) {
      return strcasecmp($a["file"],$b["file"]);      
    }
}   // ---------------------------- end of class ------------------------------
